#!/bin/bash

# any errors should cause the job to fail
set -e

PYTHON_ENV="/home/cicd/CICD"
INVENTORY_FILE="aci-inventory"
PLAYBOOK_FILE="ansible/aci.yml"

# LIFECYCLE is something like "test" or "prod"
LIFECYCLE=""

function usage() {
  echo
  echo "Usage: ${0} -l <LIFECYCLE>"
  echo
  echo " LIFECYCLE is something like 'test' or 'prod' "
  echo
}


function run_tasks() {
    source "${PYTHON_ENV}/bin/activate"

    # ./tests/setup_simulation.py
    ansible-playbook -e "ansible_python_interpreter=${PYTHON_ENV}/bin/python aci_username=${ACI_USERNAME} aci_password=${ACI_PASSWORD} lifecycle=${LIFECYCLE}" -i "${INVENTORY_FILE}" "${PLAYBOOK_FILE}"
}

# below looks complicated but it just runs the ansible test playbook up to 8 times waiting 5 secs in between
# for i in $(seq 1 8)
# do 
#     ansible-playbook -e ansible_python_interpreter=/var/lib/venvs/ansible2.2/bin/python -i gen/inventory_test_simulation ansible/test.yml && ex=0 && break || ex=$? && sleep 5
# done
# (exit $ex)

while getopts ":l:" opt
do
  case $opt in
  l)
    LIFECYCLE=$OPTARG
    ;;
  \?)
    echo "Invalid option: -${OPTARG}"
    usage
    exit 1
    ;;
  :)
    echo "Option -${OPTARG} requires an argument. "
    usage
    exit 1
    ;;
  esac
done

if [[ -z "${LIFECYCLE}" ]]
then
  echo "Not all arguments provided. "
  usage
  exit 1
fi

run_tasks